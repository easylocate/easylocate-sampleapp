# EasyLocateSample

Sample project to demonstrate the usage of the EasyLocate Android SDK API in Java and Kotlin. Please note that the EasyLocate SDK itself is written in Kotlin and this language is considered as the primary target for future apps.

**Attention: Significant API Changes with Version 0.3.x**

## Setup the SDK
### Installation of Android Studio

Currently tested with Android Studio Dolphin | 2021.3.1. You can download it [here](https://developer.android.com/studio).

###  Sample Project
The Sample Project is provided with two variants: Kotlin as well as Java reference code. You can choose your variant by selecting the according buid variant. 

1. Use the file dialog to import this sample project: "File" -> "Open...". Press then on "Sync Project with Gradle Files" to finish the project setup.
2. Use the build variant selector to choose your reference code. Take a look into the official documentation [here](https://developer.android.com/studio/run#changing-variant) and the screenshot below to select the variant you prefer.

![select_build_variant.png](docs/select_build_variant.png)

### Own App from Scratch

Follow the official Android guidelines to setup a new[ Android App project](https://developer.android.com/training/basics/firstapp/creating-project).

#### Gradle Adaptions

1. Add the EasyLocate Maven Repository to your build.gradle:

``` gradle
repositories {
	maven {
        url 'https://gitlab.com/api/v4/projects/26513304/packages/maven'
        credentials(HttpHeaderCredentials) {
            name = 'Deploy-Token'
            value = 'BPmFUJxyLQZWrDg9fyms'
        }
        authentication {
            header(HttpHeaderAuthentication)
        }
    }
}
```

2. Add the EasyLocate packages to your code dependencies:

``` gradle
dependencies {
    def easylocate_version= "0.3.0"
    implementation "de.easylocate:core:$easylocate_version"
    implementation "de.easylocate:android-sdk:$easylocate_version"
}
```

#### Android Manifest

You need to give Bluetooth and foreground service permissions to your app. This is required to keep the positioning service also in the background alive.

```
<uses-permission android:name="android.permission.BLUETOOTH" />
<uses-permission android:name="android.permission.FOREGROUND_SERVICE" />
```

## Code Usage

### Initialization of the SDK

With the initialization of the SDK you get can achieve following:

- Register the SDK with the lifecycle of the app. App events, such as onResume or onDestroy, are handled by the SDK automatically.
- Get informed about missing necessary permissions.
- Retrieve the TRACElet API object after successful initialization

Java-Code:
``` java
private EasyLocateSdk easyLocateSdk; // Is initialized in onCreate()
private EasyLocateSdk.EventListener sdkEventListener = new EasyLocateSdk.EventListener() {

        @Override
        public void onMissingRights(@NonNull List<String> list) {
            // explained later
        }

        @Override
        public void onApiReady(@NonNull CommonApi commonApi) {
            // With ths call back you get the TRACElet API object that gives you access to the position information
            traceletApi = new TraceletApi(commonApi);
            traceletApi.registerEventListener(elApiEventListener); // This event listener is explained later
        }
    };

@Override
protected void onCreate(Bundle savedInstanceState) {
    // your app init code...
    
    // Create the SDK and register it as lifecycle observer to bound it to your app's lifecycle
    easyLocateSdk = new EasyLocateSdk(this); 
    getLifecycle().addObserver(easyLocateSdk);
    easyLocateSdk.start(sdkEventListener);
}
```

Kotlin-Code:
```kotlin
private lateinit var easyLocateSdk: EasyLocateSdk // is initialized in onCreate
    private val sdkEventListener: EventListener = object : EventListener {
        override fun onMissingRights(missingRights: List<String>) {
            // explained later
        }

        override fun onApiReady(commonApi: CommonApi) {
            // With ths call back you get the TRACElet API object that gives you access to the position information
            traceletApi = TraceletApi(commonApi)
            traceletApi!!.registerEventListener(elApiEventListener)
        }
    }
}

override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    // your app init code...
    
    // Create the SDK and register it as lifecycle observer to bound it to your app's lifecycle
    easyLocateSdk = EasyLocateSdk(this)
    lifecycle.addObserver(easyLocateSdk)
    easyLocateSdk.start(sdkEventListener)
}
```

### User permissions

You also need to ask your users for fine location access. If this permission is not given, the Bluetooth Low Energy (BLE) scans cannot work and you won't be able to explore and connect EasyLocate tags.

Use the following built in code of the SDK or the reference code from the offical  [Android Developer Documentation](https://developer.android.com/training/permissions/requesting#allow-system-manage-request-code):

Java-Code:
``` java
private EasyLocateSdk.EventListener sdkEventListener = new EasyLocateSdk.EventListener() {

    @Override
    public void onMissingRights(@NonNull List<String> list) {
        // You get a list of String identifiers of the missing permissions and adapters
        if(!list.isEmpty()) {
            // You have the possibility to use built in functions of the SDK to retrieve the necessary permissions and interfaces
            easyLocateSdk.requestPermissions(list);
            easyLocateSdk.requestInterfaces(list);
        }
    }
}
```
Kotlin-Code:
``` kotlin
private val sdkEventListener: EventListener = object : EventListener {
    override fun onMissingRights(missingRights: List<String>) {
        // You get a list of String identifiers of the missing permissions and adapters
        if (missingRights.isNotEmpty()) {
            // You have the possibility to use built in functions of the SDK to retrieve the necessary permissions and interfaces
            easyLocateSdk.requestPermissions(missingRights)
            easyLocateSdk.requestInterfaces(missingRights)
        }
    }
}
```

### Register for API events

To update the app about scanning results and connection state updates an ApiEventListener must be implemented. The listener is registered with the TraceletApi object.

Java-Code:
``` java
private ApiEventListener elApiEventListener = new ApiEventListener() {

        @Override
        public void onDeviceApproached(@NonNull EasyLocateBleDevice easyLocateBleDevice) {
            // Is called when during a BLE scan (see API usage in the next section), when a TRACElet is nearby your Android devices (approx. 30 cm)
        }

        @Override
        public void onDeviceScanResults(@NonNull List<EasyLocateBleDevice> list) {
            // Is called during BLE scan and delivers all visible EasyLocate devices
        }

        @Override
        public void onNewApiState(@NonNull EasyLocateState easyLocateState) {
            lastState = easyLocateState;
            switch (lastState) {
                case IDLE:
                    // API is not yet ready
                    break;
                case INIT:
                    // API is ready, but not connected to a device
                    break;
                case CONNECTED:
                    // API is connected to a TRACElet. Positioning can start...
                    break;
            }
        }
    };

```

Kotlin-Code:
```kotlin
private val elApiEventListener: ApiEventListener = object : ApiEventListener {

    override fun onDeviceApproached(device: EasyLocateBleDevice) {
         // Is called when during a BLE scan (see API usage in the next section), when a TRACElet is nearby your Android devices (approx. 30 cm)
    }

    override fun onDeviceScanResults(easyLocateDevices: List<EasyLocateBleDevice>) {
        // Is called during BLE scan and delivers all visible EasyLocate devices
    }

    override fun onNewApiState(easyLocateState: EasyLocateState) {
        lastState = easyLocateState
        when (lastState) {
            EasyLocateState.IDLE -> {
                // API is not yet ready
            }
            EasyLocateState.INIT -> {
                // API is ready, but not connected to a device
            }
            EasyLocateState.CONNECTED -> {
                // API is connected to a TRACElet. Positioning can start...
            }
        }
    }
}

```

### Use the EasyLocate API

The API has following main functions:

- Scanning for EasyLocate devices
- Connect to one of these devices and also disconnect
- Start or stop positioning

Java-Code:
``` java
private TraceletApi traceletApi; // Initialization is performed in onApiReady of the EasyLocateSdk.EventListener

traceletApi.registerEventListener(elApiEventListener); // Register the ApiEventListener (previous section) to get informed about scan and connection results

// Start scanning
traceletApi.startDeviceScan();
// Stop scanning
traceletApi.stopDeviceScan();
// Connect to a device. Take the easyLocateBleDevice from onDeviceApproached or onDeviceScanResults
traceletApi.connectDevice(easyLocateBleDevice.getDeviceAddress());
// Disconnect from existing device connection
traceletApi.disconnectDevice();
// Start positioning. The positionUpdateListener is explained below.
traceletApi.startPositioning(positionUpdateListener);
// Stop positioning
traceletApi.stopPositioning();


```

Kotlin-Code:
```kotlin
private var traceletApi: TraceletApi? = null // Initialization is performed in onApiReady of the EasyLocateSdk.EventListener

traceletApi!!.registerEventListener(elApiEventListener) // Register the ApiEventListener (previous section) to get informed about scan and connection results

// Start scanning
traceletApi?.startDeviceScan()
// Stop scanning
traceletApi?.stopDeviceScan()
// Connect to a device. Take the easyLocateBleDevice from onDeviceApproached or onDeviceScanResults
traceletApi?.connectDevice(easyLocateBleDevice.getDeviceAddress())
// Disconnect from existing device connection
traceletApi?.disconnectDevice()
// Start positioning. The positionUpdateListener is explained below.
traceletApi.startPositioning(positionUpdateListener)
// Stop positioning
traceletApi?.stopPositioning()



```

Register for positioning results and updates:

Java-Code:
``` java
private final PositionUpdateListener positionUpdateListener = new PositionUpdateListener() {

        @Override
        public void onPositionLocalUpdate(@NonNull PositionLocal positionLocal) {
            // Use the local position
        }

        @Override
        public void onPositionWgs84Update(@NonNull PositionWgs84 positionWgs84) {
            // This will only called when a Wgs84Reference is set in the API object. See below.
            // Use the WGS84 position
        }
    
       // Further EXPERIMENTAL methods -> IGNORE THEM
 ```

Kotlin-Code:
```kotlin 
private val positionUpdateListener: PositionUpdateListener = object : PositionUpdateListener {
    override fun onPositionLocalUpdate(positionLocal: PositionLocal) {
        // Use the local position
    }

    override fun onPositionWgs84Update(positionWgs84: PositionWgs84) {
        // This will only called when a Wgs84Reference is set in the API object. See below.
        // Use the WGS84 position
    }
}
````

Enable [WGS84](https://epsg.io/4326) coordinates in the SDK. This can be done by providing a map of String IDs (so-called Site-IDs that are provided by the positioning system) to a WGS84-Reference of the local coordinate system. This reference can be automatically determined by using the EasyPlan Tool or by manually defining it. Its parameters are:

- Reference latitude and longitude: WGS84 coordinates of the origin of the local coordinates system
- Reference azimuth: This describes the angle in degrees between the north direction and the y axis of the local coordinate system. The orientation direction is clockwise.

Java-Code:
``` java
Map<String, Wgs84Reference> wgs84References = new HashMap<>();
wgs84References.put("0xea51", new Wgs84Reference(50.8358893, 12.9232455, 0.0));
traceletApi.setConfigWgs84Reference(wgs84References);
 ```
 
 Kotlin-Code:
 ``` kotlin
val wgs84References = mutableMapOf<String, Wgs84Reference>()
wgs84References["0xea51"] = Wgs84Reference(50.8358893, 12.9232455, 0.0)
traceletApi!!.setConfigWgs84Reference(wgs84References)
 ```

The position objects have following information:

- X: coordinate (PositonLocal) in meters or Latitude in degrees (PositionWgs84)
- Y: coordinate (PositonLocal) in meters or Longitude in degrees (PositionWgs84)
- Z: coordinate in meters
- Acc: accuracy of the position in meters, usually required for plotting the accuracy circle
- CovXx: covariance of the xx component  in meters
- CovXy: covariance of the xy component  in meters
- CovYy: covariance of the yy component  in meters

