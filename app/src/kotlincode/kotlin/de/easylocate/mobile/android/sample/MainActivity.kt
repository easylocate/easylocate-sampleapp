package de.easylocate.mobile.android.sample


import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import android.widget.ToggleButton
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import de.easylocate.mobile.api.*
import de.easylocate.mobile.data.coords.Wgs84Reference
import de.easylocate.mobile.data.position.PositionLocal
import de.easylocate.mobile.data.position.PositionWgs84
import de.easylocate.sdk.android.service.EasyLocateSdk
import de.easylocate.sdk.android.service.EasyLocateSdk.EventListener

class MainActivity : AppCompatActivity() {
    private lateinit var mReceivedMsgView: TextView
    private lateinit var mToggleBtnConnect: ToggleButton
    private lateinit var mToggleBtnPositioning: ToggleButton
    private var mDialog: AlertDialog? = null


    private lateinit var easyLocateSdk: EasyLocateSdk // is initialized in onCreate
    private val sdkEventListener: EventListener = object : EventListener {
        override fun onMissingRights(missingRights: List<String>) {
            if (missingRights.isNotEmpty()) {
                easyLocateSdk.requestPermissions(missingRights)
                easyLocateSdk.requestInterfaces(missingRights)
            }
        }

        override fun onApiReady(commonApi: CommonApi) {
            // With ths call back you get the TRACElet API object that gives you access to the position information
            traceletApi = TraceletApi(commonApi)
            traceletApi!!.registerEventListener(elApiEventListener)
            val wgs84References = mutableMapOf<String, Wgs84Reference>()
            wgs84References["0xea51"] = Wgs84Reference(50.8358893, 12.9232455, 0.0)
            traceletApi!!.setConfigWgs84Reference(wgs84References)
        }
    }

    private var traceletApi: TraceletApi? = null
    private var lastState = EasyLocateState.INIT


    private val elApiEventListener: ApiEventListener = object : ApiEventListener {

        override fun onDeviceApproached(device: EasyLocateBleDevice) {
            traceletApi!!.stopDeviceScan()
            traceletApi!!.connectDevice(device.deviceAddress)
            if (mDialog != null) {
                mDialog!!.dismiss()
            }
            Toast.makeText(this@MainActivity, "Tag Connected", Toast.LENGTH_SHORT).show()
        }

        override fun onDeviceScanResults(easyLocateDevices: List<EasyLocateBleDevice>) {
            // This methode delivers all scanned EasyLocate devices
        }
        override fun onNewApiState(easyLocateState: EasyLocateState) {
            lastState = easyLocateState
            when (lastState) {
                EasyLocateState.IDLE, EasyLocateState.INIT -> {
                    mToggleBtnConnect.isChecked = false
                    mToggleBtnPositioning.isChecked = false
                    mToggleBtnPositioning.isActivated = false
                }
                EasyLocateState.CONNECTED -> {
                    mToggleBtnConnect.isChecked = true
                    mToggleBtnPositioning.isActivated = true
                }
            }
        }
    }

    private val positionUpdateListener: PositionUpdateListener = object : PositionUpdateListener {
        override fun onPositionLocalUpdate(positionLocal: PositionLocal) {
            runOnUiThread {
                mReceivedMsgView.append(
                    """ New Local Position: x = ${positionLocal.x} y = ${positionLocal.y}
"""
                )
            }
        }

        override fun onPositionWgs84Update(positionWgs84: PositionWgs84) {
            // this will only called when a Wgs84Reference is set in the API object
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Create the SDK and register it as lifecycle observer to bound it to your app's lifecycle
        easyLocateSdk = EasyLocateSdk(this)
        lifecycle.addObserver(easyLocateSdk)
        easyLocateSdk.start(sdkEventListener)


        mReceivedMsgView = findViewById(R.id.receivedMsgView)
        mToggleBtnConnect = findViewById(R.id.toggleBtnConnect)
        mToggleBtnPositioning = findViewById(R.id.togglebtnPositioning)

        // Scan for the TRACElet and connect to it when approached
        mToggleBtnConnect.setOnClickListener(View.OnClickListener { v: View ->
            if (traceletApi == null) return@OnClickListener   //API not yet ready
            val toggleButton = v as ToggleButton
            if (toggleButton.isChecked) {
                // create dialog for tag
                traceletApi!!.startDeviceScan()
                mDialog = AlertDialog.Builder(this)
                    .setTitle("Approach EasyLocate Tag")
                    .setMessage("Hold the tag onto your mobile device!")
                    .create()
                mDialog!!.setCancelable(false)
                mDialog!!.show()
            } else {
                traceletApi!!.disconnectDevice()
            }
        })

        mToggleBtnPositioning.setOnClickListener(View.OnClickListener { v: View ->
            if (lastState != EasyLocateState.CONNECTED) {
                Toast.makeText(this, "Tag has to be connected!", Toast.LENGTH_SHORT).show()
                mToggleBtnPositioning.isChecked = false
                return@OnClickListener   // EasyLocate service has to be running
            }
            val toggleButton = v as ToggleButton
            if (toggleButton.isChecked) {
                // create dialog for tag
                traceletApi!!.startPositioning(positionUpdateListener)
            } else {
                traceletApi!!.stopPositioning()
            }
        })
    }
}