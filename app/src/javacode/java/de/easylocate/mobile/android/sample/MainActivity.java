package de.easylocate.mobile.android.sample;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import de.easylocate.mobile.api.ApiEventListener;
import de.easylocate.mobile.api.CommonApi;
import de.easylocate.mobile.api.EasyLocateBleDevice;
import de.easylocate.mobile.api.EasyLocateState;
import de.easylocate.mobile.api.PositionUpdateListener;
import de.easylocate.mobile.api.TraceletApi;
import de.easylocate.mobile.data.coords.Wgs84Reference;
import de.easylocate.mobile.data.position.PositionLocal;
import de.easylocate.mobile.data.position.PositionWgs84;
import de.easylocate.sdk.android.service.EasyLocateSdk;


import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;


import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    private TextView mReceivedMsgView;
    private ToggleButton mToggleBtnConnect;
    private ToggleButton mToggleBtnPositioning;
    private AlertDialog mDialog;


    // EasyLocate SDK  attributes and logic
    private EasyLocateSdk easyLocateSdk;
    private TraceletApi traceletApi;
    private EasyLocateState lastState = EasyLocateState.INIT;

    private EasyLocateSdk.EventListener sdkEventListener = new EasyLocateSdk.EventListener() {

        @Override
        public void onMissingRights(@NonNull List<String> list) {
            // You get a list of String identifiers of the missing permissions and adapters
            if(!list.isEmpty()) {
                // You have the possibility to use built in functions of the SDK to retrieve the necessary permissions and interfaces
                easyLocateSdk.requestPermissions(list);
                easyLocateSdk.requestInterfaces(list);
            }
        }

        @Override
        public void onApiReady(@NonNull CommonApi commonApi) {
            // with ths call back you get the TRACElet API object that gives you access to the position information
            traceletApi = new TraceletApi(commonApi);
            traceletApi.registerEventListener(elApiEventListener); // This event listener is explained later
            Map<String, Wgs84Reference> wgs84References = new HashMap<>();
            wgs84References.put("0xea51", new Wgs84Reference(50.8358893, 12.9232455, 0.0));
            traceletApi.setConfigWgs84Reference(wgs84References);
        }
    };

    private ApiEventListener elApiEventListener = new ApiEventListener() {

        @Override
        public void onDeviceApproached(@NonNull EasyLocateBleDevice easyLocateBleDevice) {
            traceletApi.stopDeviceScan();
            traceletApi.connectDevice(easyLocateBleDevice.getDeviceAddress());
            if(mDialog != null) {
                mDialog.dismiss();
            }
            Toast.makeText(MainActivity.this, "Tag Connected", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onDeviceScanResults(@NonNull List<EasyLocateBleDevice> list) {

        }

        @Override
        public void onNewApiState(@NonNull EasyLocateState easyLocateState) {
            lastState = easyLocateState;
            switch (lastState) {
                case IDLE:
                case INIT:
                    mToggleBtnConnect.setChecked(false);
                    mToggleBtnPositioning.setChecked(false);
                    mToggleBtnPositioning.setActivated(false);
                    break;
                case CONNECTED:
                    mToggleBtnConnect.setChecked(true);
                    mToggleBtnPositioning.setActivated(true);
                    break;
            }
        }
    };

    private final PositionUpdateListener positionUpdateListener = new PositionUpdateListener() {

        @Override
        public void onPositionLocalUpdate(@NonNull PositionLocal positionLocal) {
            runOnUiThread(() -> mReceivedMsgView.append(" New Local Position: x = " + positionLocal.getX() +
                    " y = " + positionLocal.getY() + "\n"));
        }

        @Override
        public void onPositionWgs84Update(@NonNull PositionWgs84 positionWgs84) {
            // this will only called when a Wgs84Reference is set in the API object
        }

        @Override
        public void onAssetsLocalDropped(@NonNull List<PositionLocal> list) {
            // EXPERIMENTAL: DON'T USE IT
        }

        @Override
        public void onAssetsLocalPaired(@NonNull List<PositionLocal> list) {
            // EXPERIMENTAL: DON'T USE IT

        }

        @Override
        public void onAssetsLocalUpdate(@NonNull List<PositionLocal> list) {
            // EXPERIMENTAL: DON'T USE IT

        }

        @Override
        public void onAssetsWgs84Dropped(@NonNull List<PositionWgs84> list) {
            // EXPERIMENTAL: DON'T USE IT

        }

        @Override
        public void onAssetsWgs84Paired(@NonNull List<PositionWgs84> list) {
            // EXPERIMENTAL: DON'T USE IT

        }

        @Override
        public void onAssetsWgs84Update(@NonNull List<PositionWgs84> list) {
            // EXPERIMENTAL: DON'T USE IT

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // register SDK as lifecycle observer to
        easyLocateSdk = new EasyLocateSdk(this);
        getLifecycle().addObserver(easyLocateSdk);
        easyLocateSdk.start(sdkEventListener);

        mReceivedMsgView = findViewById(R.id.receivedMsgView);
        mToggleBtnConnect = findViewById(R.id.toggleBtnConnect);
        mToggleBtnPositioning = findViewById(R.id.togglebtnPositioning);

        mToggleBtnConnect.setOnClickListener(v -> {
            if(traceletApi == null) return; //API not yet ready



            ToggleButton toggleButton = (ToggleButton) v;
            if(toggleButton.isChecked()) {
                // create dialog for tag
                traceletApi.startDeviceScan();

                mDialog = new AlertDialog.Builder(this)
                        .setTitle("Approach EasyLocate Tag")
                        .setMessage("Hold the tag onto your mobile device!")
                        .create();
                mDialog.setCancelable(false);
                mDialog.show();

            } else {
                traceletApi.disconnectDevice();
            }

        });

        mToggleBtnPositioning.setOnClickListener(v -> {
            if( lastState != EasyLocateState.CONNECTED) {
                Toast.makeText(this, "Tag has to be connected!", Toast.LENGTH_SHORT).show();
                mToggleBtnPositioning.setChecked(false);
                return;
            }

            ToggleButton toggleButton = (ToggleButton) v;
            if(toggleButton.isChecked()) {
                traceletApi.startPositioning(positionUpdateListener);
            } else {
                traceletApi.stopPositioning();
            }
        });
    }
}
